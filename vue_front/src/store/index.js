import Vue from 'vue'
import Vuex from 'vuex'
import {auth} from "./module/auth";
import {category} from "./module/category";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: auth,
    category: category
  }
})

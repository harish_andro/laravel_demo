import {loadUser} from "../../api";

export const auth = {
    state: {
        user: null
    },
    mutations: {
        setAuth: (state, user) => {
            state.user = user;
        },
        authLogout: (state)=>{
            localStorage.removeItem('v-token');
            state.user = null;
        }
    },
    actions: {
        authSuccess: (context, user=null)=>{
            if(user === null) {
                loadUser().then(value => {
                    context.commit('setAuth',value.data);
                    // eslint-disable-next-line no-unused-vars
                }).catch(reason => {
                    context.commit('authLogout');
                })
            }else{
                context.commit('setAuth',user);
            }
        },
    },
    getters: {
        isAuthenticated: state=>state.user!=null || localStorage.getItem('v-token')!=null
    }
}
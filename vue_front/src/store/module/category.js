import {loadCategories} from "../../api";

export const category = {
    state: {
        categories: []
    },
    mutations: {
        setCategories: (state,cats)=>{
            state.categories = cats;
        }
    },
    actions: {
        loadCategories: (context)=>{
            loadCategories().then(value => {
                context.commit('setCategories',value.data);
            }).catch(reason => {
                console.log(reason);
            });
        }
    },
    getters: {}
}
import axios from 'axios';

export const getClient = () => {
    let token = localStorage.getItem('v-token');
    return axios.create({
        baseURL: 'http://localhost/laravel_demo/public/api',
        headers: {'Authorization': 'Bearer ' + token}
    })
};

export const doLogin = (email,password)=>{
    return getClient().post("/login",{
        email: email,
        password: password
    });
}

export const loadUser = ()=> getClient().get('/user');

export const loadCategories = ()=>getClient().get('/category');


export const api = getClient();
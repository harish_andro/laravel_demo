export default class Validator {
    constructor(fieldName='This field') {
        this.rules = [];
        this.fieldName = (s => s.charAt(0).toUpperCase() + s.slice(1))(fieldName);
    }

    required() {
        this.rules.push(
            v => !!v || `${this.fieldName} is required`
        );
        return this;
    }

    email() {
        this.rules.push(
            v => /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/
                .test(v) || `${this.fieldName} must be valid email`
        );
        return this;
    }

    make() {
        return this.rules;
    }
}
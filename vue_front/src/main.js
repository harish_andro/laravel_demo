import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

//load if auth
if(localStorage.getItem('v-token'))
  store.dispatch('authSuccess');

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

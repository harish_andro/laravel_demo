import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from "../views/About";
import Login from "../views/Login";
import CategoryList from "../views/category/CategoryList";
import Dashboard from "../views/Dashboard";
import store from '../store'
import CategoryNew from "../views/category/CategoryNew";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/dashboard',
    component: Dashboard,
    meta: {
      reqAuth: true
    }
  },
  {
    path: '/category',
    component: CategoryList,
    meta:{
      reqAuth: true
    }
  },
  {
    path: '/category/new',
    component: CategoryNew,
    meta:{
      reqAuth: true
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if(to.meta.reqAuth){
    if(store.getters.isAuthenticated){
      next();
    }else {
      next("/login");
    }
  }
  next();
})

export default router

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/category', 'CategoryController@index')->name('category.index');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::post('/category', 'CategoryController@store')->name('category.store');
Route::delete('/category/{category}', 'CategoryController@destroy')->name('category.destroy');
Route::get('/category/{category}/edit', 'CategoryController@edit')->name('category.edit');
Route::put('/category/{category}', 'CategoryController@update')->name('category.update');
Route::get('/category/{category}', 'CategoryController@show')->name('category.show');*/

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

Route::view('/react/{any?}', 'layouts.app_react')->where('any','.*');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/category/data', 'CategoryController@data')->name('category.data');
    Route::resource('/category', 'CategoryController');
    Route::group(
        [
            'prefix' => 'products',
        ], function () {

        Route::get('/', 'ProductsController@index')
            ->name('products.product.index');

        Route::get('/create', 'ProductsController@create')
            ->name('products.product.create');

        Route::get('/show/{product}', 'ProductsController@show')
            ->name('products.product.show')
            ->where('id', '[0-9]+');

        Route::get('/{product}/edit', 'ProductsController@edit')
            ->name('products.product.edit')
            ->where('id', '[0-9]+');

        Route::post('/', 'ProductsController@store')
            ->name('products.product.store');

        Route::put('product/{product}', 'ProductsController@update')
            ->name('products.product.update')
            ->where('id', '[0-9]+');

        Route::delete('/product/{product}', 'ProductsController@destroy')
            ->name('products.product.destroy')
            ->where('id', '[0-9]+');

    });
});

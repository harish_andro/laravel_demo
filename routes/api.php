<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*auth*/
Route::post('login', 'Api\SecurityController@login');
Route::post('register', 'Api\SecurityController@signup');
/******/
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::resource('category','Api\CategoryApiController');
    Route::resource('product','Api\ProductApiController');
});
/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


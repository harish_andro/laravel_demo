import axios from "axios";

function getClient() {
    let token = localStorage.getItem("token");
    return axios.create({
        baseURL: main_url + "api",
        headers: {'Authorization': 'Bearer ' + token}
    });
}

export function doLogin(data) {
    return getClient().post("/login", data);
}

export function getCategories() {
    return getClient().get("/category");
}

export function insertCategory(data) {
    return getClient().post("/category", data);
}

export function deleteCategory(id) {
    return getClient().delete("/category/" + id);
}

export function updateCategory(id, category) {
    return getClient().put("/category/" + id, category);
}

///////////////////////////////////////////////////////////////////////

export function getProducts() {
    return getClient().get("/product");
}
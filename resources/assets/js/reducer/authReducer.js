function authReducer(currAuthData=false,action) {
    switch (action.type){
        case 'AUTH_SUCCESS':
            return true;
        case 'AUTH_FAILED':
            return false;
        default:
            return currAuthData;
    }
}

export default authReducer;
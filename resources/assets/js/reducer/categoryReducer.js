function categoryReducer(categories=[], action) {
    switch (action.type){
        case 'ADD_CATEGORY':
            return [...categories,action.payload];
        case 'REMOVE_CATEGORY':
            //console.log(action);
            return categories.filter((cat)=> cat.id!=action.payload.id);
        case 'UPDATE_CATEGORY':
            let newCats = categories.slice();
            for(let i=0;i<newCats.length;i++){
                if(newCats[i].id == action.payload.id){
                    newCats[i] = action.payload;
                }
            }
            return newCats;
        case 'LIST_CATEGORY':
            return action.payload;
        default:
            return categories;
    }
}

export default categoryReducer;
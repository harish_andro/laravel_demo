export default function productReducer(products = [], action) {
    switch (action.type){
        case 'LIST_PRODUCT':
            return action.payload;
        default:
            return products;
    }
}
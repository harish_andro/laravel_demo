import React from 'react';
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {connect} from "react-redux";
import {authFailed, authSuccess} from "../action/authActions";
import {doLogin} from "../api/api";
import {toast} from "react-toastify";


class Login extends React.Component {


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 col-md-offset-2">
                        <div className="panel panel-default">
                            <div className="panel-heading">Login</div>

                            <div className="panel-body">
                                <Formik initialValues={{email: '', password: ''}}
                                        validationSchema={
                                            Yup.object().shape({
                                                email: Yup.string().email('Invalid email address').required('Email is required!'),
                                                password: Yup.string().required('Password is required.').min(4, "Password must be at least 4 character."),
                                            })
                                        }
                                        onSubmit={(values, {setSubmitting}) => {
                                            doLogin(values).then((response) => {
                                                toast.success("Login Success");
                                                localStorage.setItem("token",response.data.token);
                                                this.props.authSuccess();
                                                setSubmitting(false);
                                                this.props.history.push("/");
                                            }, (error) => {
                                                toast.error("Invalid email or password");
                                                this.props.authFailed();
                                                setSubmitting(false);
                                            });
                                        }}
                                        render={({errors, status, touched, isSubmitting}) => {
                                            return (
                                                <Form className={"form-horizontal"}>

                                                    <div
                                                        className={"form-group " + (errors.email && touched.email ? "has-error" : "")}>
                                                        <label className="col-md-4 control-label">E-Mail</label>

                                                        <div className="col-md-6">
                                                            <Field id="email" type="email" className="form-control"
                                                                   name="email"/>
                                                            <ErrorMessage name="email" component="span"
                                                                          className="help-block"/>
                                                        </div>
                                                    </div>

                                                    <div
                                                        className={"form-group " + (errors.password && touched.password ? "has-error" : "")}>
                                                        <label className="col-md-4 control-label">Password</label>

                                                        <div className="col-md-6">
                                                            <Field id="password" type="password" autoComplete="off"
                                                                   className="form-control"
                                                                   name="password"/>
                                                            <ErrorMessage name="password" component="span"
                                                                          className="help-block"/>
                                                        </div>
                                                    </div>

                                                    <div className="form-group">
                                                        <div className="col-md-8 col-md-offset-4">
                                                            <button type="submit" className="btn btn-primary"
                                                                    disabled={isSubmitting}>
                                                                Login
                                                            </button>

                                                        </div>
                                                    </div>
                                                </Form>
                                            )
                                        }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}

export default connect(function (state) {
    return {
        isAuthenticated: state.isAuthenticated
    }
}, function (dispatch) {
    return {
        authSuccess: () => dispatch(authSuccess()),
        authFailed: () => dispatch(authFailed())
    }
})(Login);
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Login from "./Login";
import Category from "./category/ListCategory";
import PrivateRoute from "../route/PrivateRoute";
import rootReducer from '../reducer/index';
import {createStore,applyMiddleware} from "redux";
import {connect, Provider} from "react-redux";
import Home from "./Home";
import Header from "./Header";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import {ToastContainer} from "react-toastify";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faIgloo } from '@fortawesome/free-solid-svg-icons';
import {authSuccess} from "../action/authActions";
import AddCategory from "./category/AddCategory";
import EditCategory from "./category/EditCategory";
import NotFound from "./NotFound";
import ListProduct from "./product/ListProduct";


/*font awesome*/
library.add(faIgloo);

/*create redux store*/
const store = createStore(rootReducer);
if(localStorage.getItem("token")!=null){
    store.dispatch(authSuccess());
}

class Main extends Component {
    render() {
        return (
            <BrowserRouter basename={base_url}>
                <div>
                    <Header/>
                    <ToastContainer />
                    <Switch>
                        <PrivateRoute exact path='/' component={Home} isAuthenticated={this.props.isAuthenticated}/>
                        <Route exact path='/login' component={Login}/>
                        <PrivateRoute exact path='/category' component={Category}
                                      isAuthenticated={this.props.isAuthenticated}/>
                        <PrivateRoute exact path='/category/add' component={AddCategory}
                                      isAuthenticated={this.props.isAuthenticated}/>
                        <PrivateRoute exact path='/category/:category(\d+)' component={EditCategory}
                                      isAuthenticated={this.props.isAuthenticated}/>
                        <PrivateRoute exact path='/product' component={ListProduct}
                                      isAuthenticated={this.props.isAuthenticated}/>
                        <Route exact path='*' component={NotFound}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.isAuthenticated
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

const MainPage = connect(mapStateToProps, mapDispatchToProps)(Main);
export default MainPage;

if (document.getElementById('app')) {
    ReactDOM.render(<Provider store={store}><MainPage/></Provider>, document.getElementById('app'));
}

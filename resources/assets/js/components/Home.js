import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index.es";
import * as FaIcon from "@fortawesome/free-solid-svg-icons";

const Home =  () => (
    <div className="container">
        <h3>Dashboard</h3>
        <br/>
        <div className="row">
            <div className="col-lg-3 col-sm-5">
                <div className="circle-tile ">
                    <a href="#">
                        <div className="circle-tile-heading dark-blue"><FontAwesomeIcon icon={FaIcon.faUser} size="2x"/></div>
                    </a>
                    <div className="circle-tile-content dark-blue">
                        <div className="circle-tile-description text-faded"> Categories</div>
                        <div className="circle-tile-number text-faded ">265</div>
                        <a className="circle-tile-footer" href="#">More Info <FontAwesomeIcon icon={FaIcon.faArrowCircleRight}/></a>
                    </div>
                </div>
            </div>

            <div className="col-lg-3 col-sm-5">
                <div className="circle-tile ">
                    <a href="#">
                        <div className="circle-tile-heading red"><FontAwesomeIcon icon={FaIcon.faUser} size="2x"/></div>
                    </a>
                    <div className="circle-tile-content red">
                        <div className="circle-tile-description text-faded"> Products</div>
                        <div className="circle-tile-number text-faded ">10</div>
                        <a className="circle-tile-footer" href="#">More Info <FontAwesomeIcon icon={FaIcon.faArrowCircleRight}/></a>
                    </div>
                </div>
            </div>

            <div className="col-lg-3 col-sm-5">
                <div className="circle-tile ">
                    <a href="#">
                        <div className="circle-tile-heading dark-blue"><FontAwesomeIcon icon={FaIcon.faUser} size="2x"/></div>
                    </a>
                    <div className="circle-tile-content dark-blue">
                        <div className="circle-tile-description text-faded"> Users</div>
                        <div className="circle-tile-number text-faded ">265</div>
                        <a className="circle-tile-footer" href="#">More Info <FontAwesomeIcon icon={FaIcon.faArrowCircleRight}/></a>
                    </div>
                </div>
            </div>

            <div className="col-lg-3 col-sm-5">
                <div className="circle-tile ">
                    <a href="#">
                        <div className="circle-tile-heading red"><FontAwesomeIcon icon={FaIcon.faUser} size="2x"/></div>
                    </a>
                    <div className="circle-tile-content red">
                        <div className="circle-tile-description text-faded"> Users Online</div>
                        <div className="circle-tile-number text-faded ">10</div>
                        <a className="circle-tile-footer" href="#">More Info <FontAwesomeIcon icon={FaIcon.faArrowCircleRight}/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default Home;
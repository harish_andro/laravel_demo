import React from 'react';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index.es";
/*import {faAd} from "@fortawesome/free-solid-svg-icons";*/
import * as FaIcon from "@fortawesome/free-solid-svg-icons";
import {connect} from "react-redux";
import {authFailed} from "../action/authActions";


const Header = (props) => (
    <nav className='navbar navbar-expand-md navbar-light navbar-laravel navbar-default navbar-static-top'>
        <div className="container-fluid">
            <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand" href="#">Laravel-React</a>
            </div>
            <div className="collapse navbar-collapse" id="myNavbar">
                <ul className="nav navbar-nav">
                    <li><Link to='/'>Home</Link></li>
                    <li><Link to='/category'>Category</Link></li>
                    <li><Link to='/product'>Products</Link></li>
                </ul>
                <ul className="nav navbar-nav navbar-right">
                    {!props.isAuthenticated?<li><a href="#"><FontAwesomeIcon icon={FaIcon.faUserPlus}/> Sign Up</a></li>:''}
                    {!props.isAuthenticated?<li><Link to='/login'><FontAwesomeIcon icon={FaIcon.faSignInAlt}/> Login</Link></li>:''}
                    {props.isAuthenticated? <li><a role="button" onClick={()=>{ props.logout(); }}><FontAwesomeIcon icon={FaIcon.faSignOutAlt}/> Logout</a></li>:'' }
                </ul>
            </div>
        </div>
    </nav>
);

export default connect(state => {
    return {
        isAuthenticated: state.isAuthenticated
};
}, dispatch => {
    return {
        logout:() => {
            localStorage.removeItem("token");
            dispatch(authFailed())
        }
    };
})(Header);

import React from "react";
import {connect} from 'react-redux';
import {getProducts} from "../../api/api";

class ListProduct extends React.Component {

    componentDidMount() {
        getProducts().then(response => {
            this.listCategory(response.data);
        }, error => {
            console.log(error);
        });
    }

    listCategory(data) {
        this.props.dispatch({type: 'LIST_PRODUCT', payload: data});
    }

    render() {
        return (
            <div className={"container"}>
                <h4>Products</h4>
                <div className="row">
                    {this.props.products.map(product => (
                        <div className="col-md-4" key={product.id}>
                            <div className="thumbnail" style={{backgroundColor: 'white'}}>
                                <a href={product.imagePath}>
                                    <img src={product.imagePath} alt="Lights" style={{width: '100%'}}/>
                                    <div className="caption">
                                        <p>{product.name}</p>
                                        <p>Rs. {product.price}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default connect((state) => ({products: state.products}))(ListProduct);
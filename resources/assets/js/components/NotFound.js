import React from 'react';
import '../style/style.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index.es";
import * as FaIcon from "@fortawesome/free-solid-svg-icons";

const NotFound = () => (
    <div className="not-found-container">
        <div className="flex-center position-ref full-height">
            <div className="content">
                <FontAwesomeIcon icon={FaIcon.faBan} size={"7x"}/>
                <div className="title">
                    Sorry, the page you are looking for could not be found.
                </div>
            </div>
        </div>
    </div>
);
export default NotFound;

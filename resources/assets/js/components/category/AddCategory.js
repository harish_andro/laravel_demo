import React from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {insertCategory} from "../../api/api";
import {addCategory} from "../../action/categoryActions";
import {toast} from "react-toastify";
import {connect} from "react-redux";


class AddCategory extends React.Component{
    render(){
        return (
            <div className="container">
                <div className="panel panel-default">
                    <div className="panel-heading">Add Category</div>
                    <div className="panel-body">
                        <Formik
                            initialValues={{name:''}}
                            validationSchema= {
                                Yup.object().shape({
                                    name: Yup.string().required("Category name is required")
                                })
                            }
                            onSubmit={(values,{setErrors, setSubmitting})=>{
                                //do submit category
                                insertCategory(values).then(response=>{
                                    setSubmitting(false);
                                    this.props.addCategory(response.data);
                                    toast.success("Category Saved");
                                    this.props.history.push("/category");
                                }).catch(error=>{
                                    setSubmitting(false);
                                    toast.error("error while saving category");
                                    //setErrors({name: 'Name already used.'})
                                    setErrors(error.response.data.error);
                                })
                            }}
                            render={({errors, status, touched, isSubmitting}) => {
                                return (
                                    <Form>
                                        <div className={"form-group "+ (errors.name && touched.name ? "has-error" : "")}>
                                            <label className="control-label">Category Name</label>
                                            <Field type="text" id="name" name="name" className="form-control"/>
                                            <ErrorMessage name="name" component="span"
                                                          className="help-block"/>
                                        </div>
                                        <button type="submit" className="btn btn-primary"
                                                disabled={isSubmitting}>
                                            Submit
                                        </button>
                                    </Form>
                                );
                            }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(null,dispatch=>{
    return {
        addCategory: category => dispatch(addCategory(category))
    }
})(AddCategory);
import {Component} from "react";
import React from "react";
import {deleteCategory, getCategories} from "../../api/api";
import {listCategory, removeCategory} from "../../action/categoryActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index.es";
import * as FaIcon from "@fortawesome/free-solid-svg-icons";
import {toast} from "react-toastify";

class ListCategory extends Component {
    render() {
        return (
            <div className="container">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <div className="btn-group pull-right">
                            <Link to="/category/add" className="btn btn-success"><FontAwesomeIcon
                                icon={FaIcon.faPlus}/> New</Link>
                        </div>
                        <h4>Categories</h4>
                    </div>
                    <div className="panel-body">
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.categories.map(category => {
                                return (
                                    <tr key={category.id}>
                                        <td>{category.id}</td>
                                        <td>{category.name}</td>
                                        <td>{category.created_at}</td>
                                        <td>{category.updated_at}</td>
                                        <td>
                                            <button className="btn btn-danger btn-sm" title="delete"
                                                    onClick={() => {
                                                        if (confirm('Are you sure?')) {
                                                            deleteCategory(category.id).then(response => {
                                                                if(response.status==204){
                                                                    this.props.removeCategory(category);
                                                                    toast.success("category removed");
                                                                }
                                                            }).catch(error=>{
                                                                toast.error("Unable to delete category");
                                                            });
                                                        }
                                                    }}
                                            ><FontAwesomeIcon icon={FaIcon.faTrash}/></button>
                                            {'  '}
                                            <Link to={{pathname: "/category/"+category.id, category: category}} className={"btn btn-warning btn-sm"}><FontAwesomeIcon icon={FaIcon.faPencilAlt}/></Link>
                                        </td>
                                    </tr>
                                );
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }


    componentDidMount() {
        getCategories().then(response => {
            this.props.listCategory(response.data);
        }).catch(err => {
            console.log(err);
        });
    }
}

export default connect(function (state) {
    return {
        categories: state.categories
    };
}, function (dispatch) {
    return {
        listCategory: categories => {
            dispatch(listCategory(categories));
        },
        removeCategory: category => {
            dispatch(removeCategory(category));
        }
    };
})(ListCategory);
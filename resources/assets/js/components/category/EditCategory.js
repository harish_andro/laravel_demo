import React from "react";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {insertCategory, updateCategory} from "../../api/api";
import {addCategory, editCategory} from "../../action/categoryActions";
import {toast} from "react-toastify";
import {connect} from "react-redux";


class EditCategory extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="panel panel-default">
                    <div className="panel-heading">Edit Category</div>
                    <div className="panel-body">
                        <Formik
                            initialValues={this.props.location.category}
                            validationSchema={
                                Yup.object().shape({
                                    name: Yup.string().required("Category name is required")
                                })
                            }
                            onSubmit={(values, {setErrors, setSubmitting}) => {
                                //do submit category
                                updateCategory(this.props.match.params.category, values)
                                    .then(response => {
                                        setSubmitting(false);
                                        toast.success("Category Updated");
                                        this.props.editCategory(response.data);
                                        this.props.history.push("/category");
                                    }).catch(error => {
                                    setSubmitting(false);
                                    toast.error("Unable to Update category");

                                });
                            }}
                            render={({errors, status, touched, isSubmitting}) => {
                                return (
                                    <Form>
                                        <div
                                            className={"form-group " + (errors.name && touched.name ? "has-error" : "")}>
                                            <label className="control-label">Category Name</label>
                                            <Field type="text" id="name" name="name" className="form-control"/>
                                            <ErrorMessage name="name" component="span"
                                                          className="help-block"/>
                                        </div>
                                        <button type="submit" className="btn btn-primary"
                                                disabled={isSubmitting}>
                                            Submit
                                        </button>
                                    </Form>
                                );
                            }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(null, dispatch => {
    return {
        editCategory: (category) => dispatch(editCategory(category))
    }
})(EditCategory);
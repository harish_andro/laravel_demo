export function authSuccess() {
    return {type:'AUTH_SUCCESS'};
}

export function authFailed() {
    return {type:'AUTH_FAILED'};
}

export function listCategory(categories) {
    return { type: 'LIST_CATEGORY', payload: categories}
}

export function addCategory(category) {
    return { type: 'ADD_CATEGORY', payload: category };
}

export function editCategory(category) {
    return { type: 'UPDATE_CATEGORY', payload: category };
}

export function removeCategory(category) {
    return { type: 'REMOVE_CATEGORY', payload: category}
}
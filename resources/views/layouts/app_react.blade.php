<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel React Demo</title>

    <!-- Styles -->
    <link href="{{ asset('css/appreact.css') }}" rel="stylesheet">
</head>
<body>

    <div id="app">
    </div>

    <!-- Scripts -->
    <script>
        const main_url = "{{ asset('') }}";
        const base_url = "{{ asset('') }}".replace(/^(?:\/\/|[^\/]+)*\//, "")+"react/";
    </script>
    <script src="{{ asset('js/appreact.js') }}"></script>
</body>
</html>

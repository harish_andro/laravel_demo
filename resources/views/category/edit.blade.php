@extends('layouts.app')
@section('title')
    edit category
@stop
@section('content')
    <div class="container">
        <h1>Edit category</h1>
        <form action="{{route('category.update',['id'=>$category->id])}}" method="post">
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="name">Category Name</label>
                <input type="text" name="name" id="name" class="form-control" value="{{$category->name}}">
                <span class="has-error text-danger">@if ($errors->has('name'))
                    {{$errors->first('name')}}
                @endif</span>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
            {{ csrf_field() }}
        </form>
    </div>
@stop
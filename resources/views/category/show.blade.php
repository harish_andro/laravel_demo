@extends('layouts.app')
@section('title')
    show category
@stop
@section('content')
    <div class="container">
        <h1>{{$category->name}}</h1>
        id: {{$category->id}}<br>
        created: {{$category->created_at->format('d/m/Y h:i A')}}<br>
        updated: {{$category->updated_at->format('d/m/Y h:i A')}}<br>

        <a href="{{route('category.edit',['id'=>$category->id])}}" class="btn btn-info">Edit</a>

        <form action="{{route('category.destroy',['id'=>$category->id])}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>

    </div>
@stop
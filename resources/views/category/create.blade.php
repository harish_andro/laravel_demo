@extends('layouts.app')
@section('title')
    Add category
@stop
@section('content')
    <div class="container">
        <h1>Insert category</h1>
        <form action="{{route('category.store')}}" method="post">
            <div class="form-group">
                <label for="name">Category Name</label>
                <input type="text" name="name" id="name" class="form-control">
                <span class="has-error text-danger">@if ($errors->has('name'))
                    {{$errors->first('name')}}
                @endif</span>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            {{ csrf_field() }}
        </form>
    </div>
@stop
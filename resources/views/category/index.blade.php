@extends('layouts.app')
@section('title')Category list @stop
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="h2">Category</span>
                <a class="pull-right btn btn-primary" href="{{route('category.create')}}">Add category</a>
            </div>
            <div class="panel-body">
                <table class="table table-bordered" id="">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>createdat</th>
                        <th>updated</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    {{--<tbody>--}}
                    <?php /** @var \App\Category $category */ ?>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td><b>{{$category->name}}</b></td>
                            <td>{{$category->created_at->format('d/m/Y h:i A')}}</td>
                            <td>{{$category->updated_at->format('d/m/Y h:i A')}}</td>
                            <td>
                                <a href="{{route('category.edit',['id'=>$category->id])}}">Edit</a> -
                                <a href="{{route('category.show',['id'=>$category->id])}}">Show</a>
                            </td>
                        </tr>
                    @endforeach
                    {{--</tbody>--}}
                </table>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        /*$(function () {
            $('#category_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{--{{route('category.data')}}--}}',
                columns: [
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'created_at'},
                    {data: 'updated_at'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });*/
    </script>
@stop
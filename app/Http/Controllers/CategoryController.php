<?php

namespace App\Http\Controllers;

use App\Category;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    //
    public function index()
    {
        $cat = Category::all();
        return view('category.index',['categories'=>$cat]);
    }

    public function data(DataTables $dataTables)
    {
        $query = Category::all();

        return $dataTables->collection($query)
            ->addColumn('action', function ($category){
                return '<a href='.route('category.edit',['id'=>$category->id]).'>Edit</a> - <a href='.route('category.destroy',['id'=>$category->id]).'>Delete</a>';
            })
            ->make(true);
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'name'=>'required|unique:categories,name'
        ]);
        Category::create(\request()->all());
        \Session::flash('success', 'Category saved');
        return redirect()->route('category.index');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        \Session::flash('danger', 'Category deleted');
        return redirect()->route('category.index');
    }

    public function edit(Category $category)
    {
        return view('category.edit',compact('category'));
    }

    public function update(Request $request, Category $category)
    {
    	$validate = $this->validate($request,[
            'name'=>'required|unique:categories,name,'.$category->id
        ]);
        $category->update(\request()->all());
        \Session::flash('warning', 'Category updated');
        return redirect()->route('category.index');
    }

    public function show(Category $category)
    {
        return view('category.show',compact('category'));
    }
}

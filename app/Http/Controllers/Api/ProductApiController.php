<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use mysql_xdevapi\Exception;

class ProductApiController extends Controller
{
    /**
     * Display a listing of the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with('category')->get();
    }

    /**
     * Store a newly created product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required|unique:products,name',
            'description'=>'nullable',
            'price'=>'required|numeric',
            'image'=>'required|mimes:jpeg,bmp,png,jpg',
            'category_id'=>'required|numeric'
        ];
        $data = $request->all();
        $validator = \Validator::make($data,$rules);
        if($validator->fails()){
            $msgs = [];
            $msgs['error'] = "Invalid data";
            $msgs['message'] = "Invalid data given";
            $msgs['validations'] = $validator->errors();
            return response()->json($msgs,422);
        }
        /** @var UploadedFile $img */
        $img = $data['image'];
        $data['image'] = $img->move('images',$img->getClientOriginalName())->getFilename();
        return Product::create($data);
    }

    /**
     * Display the specified product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::findOrFail($id);
    }


    /**
     * Update the specified product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $rules = [
            'name'=>'required|unique:products,name,'.$id,
            'description'=>'nullable',
            'price'=>'required|numeric',
            'image'=>'nullable|mimes:jpeg,bmp,png,jpg',
            'category_id'=>'required|numeric'
        ];
        $data = $request->all();
        $validator = \Validator::make($data,$rules);
        if($validator->fails()){
            $msgs = [];
            $msgs['error'] = "Invalid data";
            $msgs['message'] = "Invalid data given";
            $msgs['validations'] = $validator->errors();
            return response()->json($msgs,422);
        }
        /** @var UploadedFile $img */
        if($request->has('image')) {
            $img = $data['image'];
            $data['image'] = $img->move('images', $img->getClientOriginalName())->getFilename();
        }
        $product->update($data);
        return $product;
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryApiController extends Controller
{
    /**
     * Display a listing of the categories
     *
     * @return Category[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Category::all();
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'name'=>'required|unique:categories,name'
        ];
        $validator = Validator::make($request->all(),$rule);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()],422);
        }else {
            $category = Category::create($request->all());
            return response()->json($category, 201);
        }
    }

    /**
     * Display the specified category.
     *
     * @param Category $category
     * @return Category
     */
    public function show(Category $category)
    {
        return $category;
    }


    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $rule = [
            'name'=>'required|unique:categories,name,'.$category->id
        ];
        $this->validate($request,$rule);
        $category->update($request->all());
        return response()->json($category,200);
    }

    /**
     * Remove the specified category from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json(null,204);
    }
}

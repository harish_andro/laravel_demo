<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class ProductsController extends Controller
{

    /**
     * Display a listing of the products.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $products = Product::paginate(25);

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new product.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        //$categories = Category::pluck('id', 'id')->all();
        $categories = Category::all();

        return view('products.create', compact('categories'));
    }

    /**
     * Store a new product in the storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $data = $this->getData($request);
        /** @var UploadedFile $image */
        $image = $data['image'];
        /*$imageName=$image->getClientOriginalName();
        $image->move(public_path('/images'),$imageName);
        $data['image']=$imageName;*/
        $data['image'] = $image->storePublicly('prods','public');
        dd($data['image']);
        Product::create($data);

        return redirect()->route('products.product.index')
            ->with('success', 'Product was successfully added!');

    }

    /**
     * Display the specified product.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified product.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        //$categories = Category::pluck('id', 'id')->all();
        $categories = Category::all();

        return view('products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified product in the storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $data = $this->getData($request);
        if(isset($data['image'])){
            /** @var UploadedFile $image */
            $image = $data['image'];
            $imageName=$image->getClientOriginalName();
            $image->move(public_path('/images'),$imageName);
            $data['image']=$imageName;
        }

        $product = Product::findOrFail($id);
        $product->update($data);

        return redirect()->route('products.product.index')
            ->with('success', 'Product was successfully updated!');
    }

    /**
     * Remove the specified product from the storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $product = Product::findOrFail($id);
            $product->delete();

            return redirect()->route('products.product.index')
                ->with('success', 'Product was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:1|max:255',
            'description' => 'nullable',
            'price' => 'required|numeric|min:0|max:999999.99',
            'category_id' => 'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
        if($request->method()=='PUT'){
            $rules['image'] = 'nullable';
        }

        $data = $request->validate($rules);

        return $data;
    }

}

---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://192.168.0.104/laravel_demo/public/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## api/login
> Example request:

```bash
curl -X POST "http://192.168.0.104/laravel_demo/public/api/login" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/login");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/login`


<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## api/register
> Example request:

```bash
curl -X POST "http://192.168.0.104/laravel_demo/public/api/register" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/register");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/register`


<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_db20564ba266cd451caac114b3eac8ab -->
## Display a listing of the categories

> Example request:

```bash
curl -X GET -G "http://192.168.0.104/laravel_demo/public/api/category" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/category");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "error": "Unauthenticated"
}
```

### HTTP Request
`GET api/category`


<!-- END_db20564ba266cd451caac114b3eac8ab -->

<!-- START_894ef06ce7a41cb47f9c434fcd778d9a -->
## Store a newly created category in storage.

> Example request:

```bash
curl -X POST "http://192.168.0.104/laravel_demo/public/api/category" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/category");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/category`


<!-- END_894ef06ce7a41cb47f9c434fcd778d9a -->

<!-- START_977e23840a7e9249b1f7136f1eadabe2 -->
## Display the specified category.

> Example request:

```bash
curl -X GET -G "http://192.168.0.104/laravel_demo/public/api/category/1" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/category/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "error": "Unauthenticated"
}
```

### HTTP Request
`GET api/category/{category}`


<!-- END_977e23840a7e9249b1f7136f1eadabe2 -->

<!-- START_ed2985a22796532e66be664ff9783124 -->
## Update the specified category in storage.

> Example request:

```bash
curl -X PUT "http://192.168.0.104/laravel_demo/public/api/category/1" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/category/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/category/{category}`

`PATCH api/category/{category}`


<!-- END_ed2985a22796532e66be664ff9783124 -->

<!-- START_c663adad7473b698445af374c584ba20 -->
## Remove the specified category from storage.

> Example request:

```bash
curl -X DELETE "http://192.168.0.104/laravel_demo/public/api/category/1" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/category/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/category/{category}`


<!-- END_c663adad7473b698445af374c584ba20 -->

<!-- START_dc538d69a8586a7a3c36d4393cee42e6 -->
## Display a listing of the products.

> Example request:

```bash
curl -X GET -G "http://192.168.0.104/laravel_demo/public/api/product" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/product");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "error": "Unauthenticated"
}
```

### HTTP Request
`GET api/product`


<!-- END_dc538d69a8586a7a3c36d4393cee42e6 -->

<!-- START_2d62ba7cf16a7d6db447375e13e86c34 -->
## Store a newly created product in storage.

> Example request:

```bash
curl -X POST "http://192.168.0.104/laravel_demo/public/api/product" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/product");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/product`


<!-- END_2d62ba7cf16a7d6db447375e13e86c34 -->

<!-- START_1fcbf5d495e6ada99ea017e9ae32b380 -->
## Display the specified product.

> Example request:

```bash
curl -X GET -G "http://192.168.0.104/laravel_demo/public/api/product/1" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/product/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "error": "Unauthenticated"
}
```

### HTTP Request
`GET api/product/{product}`


<!-- END_1fcbf5d495e6ada99ea017e9ae32b380 -->

<!-- START_682327ab9f9deab00b7c603486ad935a -->
## Update the specified product in storage.

> Example request:

```bash
curl -X PUT "http://192.168.0.104/laravel_demo/public/api/product/1" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/product/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/product/{product}`

`PATCH api/product/{product}`


<!-- END_682327ab9f9deab00b7c603486ad935a -->

<!-- START_587b06cc0dc038b2e049f3a1baa2593b -->
## Remove the specified product from storage.

> Example request:

```bash
curl -X DELETE "http://192.168.0.104/laravel_demo/public/api/product/1" 
```

```javascript
const url = new URL("http://192.168.0.104/laravel_demo/public/api/product/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/product/{product}`


<!-- END_587b06cc0dc038b2e049f3a1baa2593b -->


